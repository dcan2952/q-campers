import { shallow } from 'enzyme'
import Index from '../pages/index'

describe('Index', () => {
  var wrapper, props
  beforeEach(async () => {
    props = await Index.getInitialProps({ query: {} })
    wrapper = shallow(<Index {...props} />)
  })

  it('shows "Brownie Points Leaderboard (Top 100)"', () => {
    expect(wrapper.find('div.title').text()).toEqual('Brownie Point Leaderboard (Top 100)')
  })

  it('gets a list of 100 campers', () => {
    expect(props.campers.length).toEqual(100)
  })

  it('sets default leaderboard to sort by brownie points over last 30 days', () => {
    expect(props.sortBy).toEqual('top-last-thirty')
  })

})
