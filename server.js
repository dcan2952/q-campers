const express = require('express')
const next = require('next')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const camperGenerator = require('./server/camperGenerator')
const camperUtil = Object.create(camperGenerator.CamperUtil)
camperUtil.generateCampers(200)

app
  .prepare()
  .then(() => {
    const server = express()

    server.get('/:type', (req, res) => {
      const actualPage = '/'
      const queryParams = { type: req.params.type }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/campers/top-last-thirty', (req, res) => {
      const campersTopLastThirty = camperUtil.getTop('lastThirtyBrowniePoints')
      res.json(campersTopLastThirty)
    })

    server.get('/campers/top-all-time', (req, res) => {
      const campersTopAllTime = camperUtil.getTop('allTimeBrowniePoints')
      res.json(campersTopAllTime)
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, err => {
      if (err) throw err
      console.log('> Ready on http://localhost:3000')
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })
