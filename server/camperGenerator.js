const faker = require('faker')

const CamperUtil = {
	campers: [],
	generateCampers: function generateCampers(num) {
		for (let i = 0; i < num; i++) {
			let camperData = {
				name: faker.name.findName(),
				lastThirtyBrowniePoints: Math.round(100*Math.random()),
				allTimeBrowniePoints: Math.round(1000*Math.random())
			}
			this.campers.push(camperData)
		}
		return this.campers
	},
	getTop: function getTop(type) {
		return [...this.campers].sort((a,b) => {
			if (a[type] === b[type]) {
				return b.name < a.name
			} else {
				return b[type] - a[type]
			}
		}).slice(0,100)
	}
}

module.exports = {
	CamperUtil: CamperUtil
}