const camperGenerator = require('./camperGenerator')
const camperUtil = Object.create(camperGenerator.CamperUtil)
camperUtil.generateCampers(200)

const app = require('express')()

app.get('/api/campers/top-last-thirty', (req, res) => {
  const campersTopLastThirty = camperUtil.getTop('lastThirtyBrowniePoints')
  res.json(campersTopLastThirty)
})

app.get('/api/campers/top-all-time', (req, res) => {
  const campersTopAllTime = camperUtil.getTop('allTimeBrowniePoints')
  res.json(campersTopAllTime)
})

app.get('*', (req, res) => {
  res.send('Express server catch-all!')
})

app.listen()
