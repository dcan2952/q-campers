import Header from '../components/Header'
import Toggle from '../components/Toggle'
import Leaderboard from '../components/Leaderboard'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'

const About = (props) => (
  <div className="app">
  	<Header />
  	<div className="hero-section" style={{backgroundImage: 'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(https://s3.amazonaws.com/imagescloud/images/medias/camping/camping-tente.jpg)'}}>
	   	<div className="title">Brownie Point Leaderboard (Top 100)</div>
	   	<Toggle sortBy={props.sortBy} />
	   	<Leaderboard campers={props.campers} sortBy={props.sortBy} />
	</div>
    <style jsx global>{`
		* {
			box-sizing: border-box;
		}
		@font-face {font-family: "CiutadellaW04-Lt";
		    src: url("//db.onlinewebfonts.com/t/0d0e90eb107486ca06f62c39a3c15f4e.eot");
		    src: url("//db.onlinewebfonts.com/t/0d0e90eb107486ca06f62c39a3c15f4e.eot?#iefix") format("embedded-opentype"),
		    url("//db.onlinewebfonts.com/t/0d0e90eb107486ca06f62c39a3c15f4e.woff2") format("woff2"),
		    url("//db.onlinewebfonts.com/t/0d0e90eb107486ca06f62c39a3c15f4e.woff") format("woff"),
		    url("//db.onlinewebfonts.com/t/0d0e90eb107486ca06f62c39a3c15f4e.ttf") format("truetype"),
		    url("//db.onlinewebfonts.com/t/0d0e90eb107486ca06f62c39a3c15f4e.svg#CiutadellaW04-Lt") format("svg");
		}
		@font-face {font-family: "CiutadellaW04-Bold";
		    src: url("//db.onlinewebfonts.com/t/6cfda2958e863ef8dc3c6ba3dd4a6ae2.eot");
		    src: url("//db.onlinewebfonts.com/t/6cfda2958e863ef8dc3c6ba3dd4a6ae2.eot?#iefix") format("embedded-opentype"),
		    url("//db.onlinewebfonts.com/t/6cfda2958e863ef8dc3c6ba3dd4a6ae2.woff2") format("woff2"),
		    url("//db.onlinewebfonts.com/t/6cfda2958e863ef8dc3c6ba3dd4a6ae2.woff") format("woff"),
		    url("//db.onlinewebfonts.com/t/6cfda2958e863ef8dc3c6ba3dd4a6ae2.ttf") format("truetype"),
		    url("//db.onlinewebfonts.com/t/6cfda2958e863ef8dc3c6ba3dd4a6ae2.svg#CiutadellaW04-Bold") format("svg");
		}
		body {
			font-family: "CiutadellaW04-Lt", "Ciutadella-Regular", "BrauerNeueRegular", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
		}
		html, body, #__next {
			margin: 0;
			height: 100%;
		}
		.hero-section {
		  height: 100%;
		  background-position: center;
		  background-repeat: no-repeat;
		  background-size: cover;
		  position: fixed;
		  left: 0;
		  right: 0;
		  padding-top: 120px;
		  overflow-y: scroll;
		}
		.hero-section .title {
			color: #fff;
			text-align: center;
		}
		.center {
			text-align: center;
		}
    `}</style>
  </div>
)

About.getInitialProps = async function(context) {
  const { type } = context.query.type ? context.query : { type: 'top-last-thirty' }
  const res = await fetch(`https://qantas-campers.dcan2952.now.sh/api/campers/${type}`)
  const data = await res.json()

  console.log(`Campers fetched. Count: ${data.length}`)

  return {
    campers: data,
    sortBy: type
  }
}

export default About
