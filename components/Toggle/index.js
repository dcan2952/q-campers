import Link from 'next/link'

const Toggle = (props) => (
	<div className="toggle-links btn-group">
		<Link href="/?type=top-last-thirty" as="/top-last-thirty">
			<label className={`btn btn-outline ` + (props.sortBy == 'top-last-thirty' ? `active` : null)}>
				<span>Last 30 days</span>
			</label>
		</Link>
		<Link href="/?type=top-all-time" as="/top-all-time">
			<label className={`btn btn-outline ` + (props.sortBy == 'top-all-time' ? `active` : null)}>
				<span>All time</span>
			</label>
		</Link>
		<style jsx>{`
			.toggle-links {
				margin-top: 30px;
				text-align: center;
			}
			.btn-group > .btn:not(:last-child), .btn-group > .btn-group:not(:last-child) > .btn {
			    border-top-right-radius: 0;
			    border-bottom-right-radius: 0;
			}
			.btn-group > .btn:last-child, .btn-group > .btn-group:last-child > .btn {
			    border-top-left-radius: 0;
			    border-bottom-left-radius: 0;
			}
			.btn-outline.active {
			    background-color: #e40000;
			    border-color: #e40000;
			    color: #fff;
			}
			.btn-group > .btn, .btn-group-vertical > .btn {
			    position: relative;
			    flex: 1 1 auto;
			}
			.btn {
				font-size: 14px;
			    padding-left: 12px;
			    padding-right: 12px;
			    cursor: pointer;
			    box-shadow: rgba(0, 0, 0, 0.06) 0px 2px 4px 0px;
			    display: inline-block;
			    font-weight: 400;
			    background-color: #fff;
			    color: #212529;
			    text-align: center;
			    vertical-align: middle;
			    user-select: none;
			    border: 1px solid transparent;
			    padding: 0.375rem 0.75rem;
			    line-height: 1.5;
			    border-radius: 0.25rem;
			    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
			}
			.btn-outline {
	    		border-color: #dce0e0;
	    		width: 98px;
	    	}
	    `}</style>
	</div>
)

export default Toggle
