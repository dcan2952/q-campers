import Link from 'next/link'

const Header = () => (
	<nav className="header">
		<Link href="/"><img src="/static/logo.jpg" alt="logo" /></Link>
		<div className="title">Qantas Campers</div>
		<style jsx>{`
	      	nav.header {
			  z-index: 1;
			  background-color: #fff;
			  position: fixed;
			  left: 0;
			  right: 0;
			  box-shadow: rgba(0, 0, 0, 0.06) 0px 2px 4px 0px;
			}
			nav.header {
			  display: flex;
			  align-items: center;
			}
			nav.header img {
			  height: 70px;
			  margin: 0 20px;
			}
			nav.header .title {
				color: #e40000;
				text-transform: uppercase;
			}
	    `}</style>
 	</nav>
)

export default Header
