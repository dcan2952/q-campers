

const Leaderboard = (props) => (
	<div className="leaderboard-container">
    	<div className="leaderboard">
    		<div className="table-head">
    			<div style={{textAlign: 'right', paddingRight: '20px'}}>Brownie points</div>
    		</div>
    		<div className="leaderboard-row table-head" style={{marginBottom: '10px'}}>
    			<div style={{paddingLeft: '30px'}}>Name</div>
    			<div className={(props.sortBy !== 'top-last-thirty' ? 'inactive' : '')} style={{minWidth:'102px'}}>Last 30 days</div>
    			<div className={(props.sortBy !== 'top-all-time' ? 'inactive' : '')}>All time</div>
    		</div>
	      {props.campers.map((camper,i) => (
	        <div key={i} className="leaderboard-row">
	        	<div><span className="rank">{i+1}.</span> {camper.name}</div>
	          	<div className={(props.sortBy === 'top-last-thirty' ? 'active' : '')}>{camper.lastThirtyBrowniePoints}</div>
	          	<div className={(props.sortBy === 'top-all-time' ? 'active' : '')}>{camper.allTimeBrowniePoints}</div>
	        </div>
	      ))}
	    </div>
	    <style jsx>{`
			.leaderboard-container {
				margin-top: 20px;
				text-align: center;
			}
			.leaderboard {
				padding: 20px 0;
				margin-bottom: 150px;
				background-color: #fff;
				border-radius: 10px;
				display: inline-block;
				width: 400px;
				max-width: 100%;
			}
			.leaderboard-row {
				display: flex;
				align-items: center;
				padding: 2px 20px;
			}
			.leaderboard-row > div {
				/* fix for free Ciutadella font files */
				position: relative;
				top: 1px;
			}
			.table-head > div:not(.inactive), .leaderboard-row > div.active  {
				font-family: "CiutadellaW04-Bold", "BrauerNeueRegular", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
			}
			.table-head {
				font-size: 14px;
			}
			.table-head > div.inactive {
				font-weight: 500;
			}
			.leaderboard-row:nth-child(odd) {
				background-color: rgba(228, 0, 0, 0.1);
			}
			.leaderboard-row > div:first-child {
				flex: 1;
				text-align: left;
			}
			.leaderboard-row > div:not(:first-child) {
				padding-left: 20px;
				min-width: 70px;
				text-align: right;
			}
			.leaderboard-row .rank {
				display:inline-block;
				min-width: 27px;
				text-align: center;
			}
	    `}</style>
	</div>
)

export default Leaderboard